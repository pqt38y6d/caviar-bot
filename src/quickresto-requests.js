const moment = require('moment');
const axios = require('axios');
const config = require('./config');


const HOST = 'https://caviarmozyr.quickresto.ru/platform/online/api/'

const instance = axios.create({
    baseURL: HOST,
    auth: {
        username: 'ke543',
        password: 'T7yjXMRd'
    },
    headers: {
        'Connection': 'keep-alive',
        'Content-type': 'application/json',
        'Host': 'caviarmozyr.quickresto.ru'
    }
});

async function getShiftList() {
    console.log('зашел в функцию получения смен')
    await instance
        .get(`list`,
            {
                params: {
                    moduleName: 'front.zreport'
                },
            })
        .then((res) => {
            // globalStorage.currentShiftId = res.data[3].frontId;
            globalStorage.ordersCount = res.data[1].ordersCount;
        })
        .catch((error) => {
            console.error(error)
        })
}


const globalStorage =
    {
        currentShiftId: '',
        ordersCount: 21,
        breakfastTotal: 0,
        barTotal: 0,
        kitchenTotal: 0,
        teaTotal: 0,
        hookahTotal: 0,
        stuffTotal: 0,
        lastOrder: 6034
    }

async function davaiVsyo() {
    console.log('зашел в функцию давайвсе')
    console.log(globalStorage);
    for (let i = 1; i <= globalStorage.ordersCount; i++) {
        await getOrdersPositions();
        globalStorage.lastOrder++;
    }
    console.log(globalStorage);
}

async function getOrdersPositions() {
    await instance
        .get(`read`,
            {
                params: {
                    moduleName: 'front.orders',
                    objectId: globalStorage.lastOrder
                },
            })
        .then((res) => {
            let orders = res.data.orderItemList;
            let totalSumByQR = res.data.frontTotalPrice;
            let orderSum = 0;
            let sumOfOrder = {
                breakfastTotal: 0,
                barTotal: 0,
                kitchenTotal: 0,
                teaTotal: 0,
                hookahTotal: 0,
                stuffTotal: 0
            }

            orders.map(item => {
                let {amount, product, totalAmount} = item;
                let sum = totalAmount;
                let price = totalAmount / amount
                let {name} = product;
                let what = {
                    name,
                    parent: whileParentExist(product),
                    price,
                    amount,
                    sum
                }
                let {parent} = what;
                if (parent === "Завтраки") {
                    sumOfOrder.breakfastTotal += sum;
                } else if (parent === "Бар") {
                    sumOfOrder.barTotal += sum;
                } else if (parent === "Кухня") {
                    sumOfOrder.kitchenTotal += sum;
                } else if (parent === "Кальяны") {
                    sumOfOrder.hookahTotal += sum;
                } else if (parent === "Чайная карта") {
                    sumOfOrder.teaTotal += sum;
                } else if (parent === "STUFF ONLY") {
                    sumOfOrder.stuffTotal += sum;
                }
                orderSum += sum;
            })
            if (orderSum === totalSumByQR) {
                globalStorage.breakfastTotal += sumOfOrder.breakfastTotal;
                globalStorage.barTotal += sumOfOrder.barTotal;
                globalStorage.kitchenTotal += sumOfOrder.kitchenTotal;
                globalStorage.hookahTotal += sumOfOrder.hookahTotal;
                globalStorage.teaTotal += sumOfOrder.teaTotal;
                globalStorage.stuffTotal += sumOfOrder.stuffTotal;

                console.log({
                    order: globalStorage.lastOrder,
                    sumOfOrder: orderSum,
                })
            } else {
                console.error(`ошибка в чеке #${globalStorage.lastOrder}`);
                // console.error(orders);
                console.error(orderSum);
                console.error(totalSumByQR);
            }
        })
        .catch((error) => {
            console.error(error)
        })
}

function whileParentExist(item) {
    while ('parentItem' in item) {
        item = item.parentItem;
    }
    return item.itemTitle;
}


module.exports.getOrdersPositions = getOrdersPositions
module.exports.getShiftList = getShiftList
module.exports.globalStorage = globalStorage
module.exports.davaiVsyo = davaiVsyo
