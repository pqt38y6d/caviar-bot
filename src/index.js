const TelegramBot = require('node-telegram-bot-api')
const config = require('./config')
const helper = require('./helper')
const calendar = require('./others/calendar')
const keyboard = require('./keyboard')
const kb = require ('./keyboard-buttons')
const quickresto = require('./quickresto-requests')
const google = require('./google-sheets-requests')
const {getShiftList, globalStorage, getOrdersPositions, davaiVsyo} = require("./quickresto-requests");
const {getGoogleData} = require("./google-sheets-requests");

const CURRENT_DATE = new Date();
let CURRENT_YEAR = CURRENT_DATE.getFullYear();
let CURRENT_MONTH = CURRENT_DATE.getMonth() + 1;


helper.logStart();


const bot = new TelegramBot(config.TOKEN, {
    polling: {
        interval: 300,
        autoStart: true,
        params: {
            timeout: 10
        }
    }
})


bot.on("polling_error", console.log);

// bot.onText(/\/start (.+)/, (msg, [source, match]) => {
//     const { chat: { id, first_name, last_name } } = msg;
//     bot.sendMessage(id, source)
// })


bot.onText(/\/start/, async msg => {
    await getShiftList();
    // await getOrdersPositions();
    await davaiVsyo();
    console.log ('я запустился')
    await getGoogleData(msg);
    // getDate(msg);
    // const chatId = helper.getChatId(msg)
    // const guestName = msg.from.first_name;
    // const welcome_new1 = [`Hey ${guestName}, Welcome to our Caviar bot`, `Howdy ${guestName}, Weclome to our Demo_Restaurant.`,
    // `Hello ${guestName}, Welcome to our Demo_Restaurant`]
    // const reply = welcome_new1[Math.floor(Math.random() * welcome_new1.length)]
    // await bot.sendMessage(chatId, reply, {
    //     reply_markup: {
    //         keyboard: keyboard.home
    //     }
    // })
    // newPerson(msg);

    const welcome_new2 = "\n It Looks like you are a New User."
    const welcom_registered = ["\n Great to have you back.", "\n Glad to see you Back."]
})

bot.onText(/^(?:[0-2][0-9]|[1-9]|3[01]).(?:0[1-9]|[1-9]|1[012]).(?:202[1-9]|2[1-9])\s(?:0[0-9]|[0-9]|1[0-9]|2[1234]):(?:[0-5][0-9])$/, msg => {
})

bot.on('message', msg => {

})
// bot.onText(/\/calendar-day-(?:[0-2][0-9]|[1-9]|3[01])-(?:0[1-9]|[1-9]|1[012])-(?:202[1-9]|2[1-9])$/, msg => {
// })


function newPerson(message, m1 = 'menu or table?') {
    const chatId = helper.getChatId(message);
    const markUp = {
        reply_markup: {
            resize_keyboard: true,
            one_time_keyboard: true,
            keyboard: [
                ['Menu', 'Reserve a table']
            ]
        }
    }
    bot.sendMessage(chatId, m1, markUp)
}

// function registeredPerson(message) {
//     const chatId = helper.getChatId(message)

// }


function getContact(msg, text) {
    bot.sendMessage(helper.getChatId(msg), text, {
        parse_mode: 'HTML',
        reply_markup: { inline_keyboard: calendar.createCalendar(CURRENT_YEAR, CURRENT_MONTH) }
    })
}

function getDate(message) {
    const chatId = helper.getChatId(message)
    const t1 = 'Здорово, давай выберем с тобой дату'
    const markup = calendar.createCalendar(CURRENT_YEAR, CURRENT_MONTH)
    bot.sendMessage(chatId, t1,
        {
            reply_markup: { inline_keyboard: markup }
        }
    )

}

bot.on('callback_query', query => {
    const msg = query.message;
    let markUp = [];
    switch (query.data) {
        case 'previous-month':
            if (CURRENT_MONTH > CURRENT_DATE.getMonth() + 1) {
                CURRENT_MONTH--;           
            markUp = calendar.createCalendar(CURRENT_YEAR, CURRENT_MONTH)
            bot.editMessageText('Здорово, давай выберем с тобой дату',
                {
                    reply_markup: {
                        inline_keyboard: markUp
                    },
                    chat_id: msg.chat.id,
                    message_id: msg.message_id
                });
            }
            break;
        case 'next-month':
            CURRENT_MONTH++;
            markUp = calendar.createCalendar(CURRENT_YEAR, CURRENT_MONTH)
            bot.editMessageText('Здорово, давай выберем с тобой дату',
                {
                    reply_markup: {
                        inline_keyboard: markUp
                    },
                    chat_id: msg.chat.id,
                    message_id: msg.message_id
                });
            break;
        // case new RegExp(/\/calendar-day-(?:[0-2][0-9]|[1-9]|3[01])-(?:0[1-9]|[1-9]|1[012])-(?:202[1-9]|2[1-9])$/)
    }
})



var bookPages = 100;

function getPagination(current, maxpage) {
    var keys = [];
    if (current > 1) keys.push({ text: `«1`, callback_data: '1' });
    if (current > 2) keys.push({ text: `‹${current - 1}`, callback_data: (current - 1).toString() });
    keys.push({ text: `-${current}-`, callback_data: current.toString() });
    if (current < maxpage - 1) keys.push({ text: `${current + 1}›`, callback_data: (current + 1).toString() })
    if (current < maxpage) keys.push({ text: `${maxpage}»`, callback_data: maxpage.toString() });

    return {
        reply_markup: JSON.stringify({
            inline_keyboard: [keys]
        })
    };
}
