const calendar = require('node-calendar')

function createCalendar(year, month) {
    if (month > 12 && month % 12 === 0) {
        year = year + checkFullNumber(month) - 1;
        month = 12
    } else {
        year = year + checkFullNumber(month);
        month = month % 12;
    }




    const markup = []
    let row = [];

    row.push({ text: `${monthNameModificator(month)} ${year}`, callback_data: 'ignore' })
    markup.push(row);

    const weekDays = ["Пн", "Вт", "Ср", "Чт", "Пт", "Сб", "Вс"]
    row = []

    weekDays.forEach(day => {
        row.push({ text: `${day}`, callback_data: 'ignore' })
    })
    markup.push(row)

    const myCalendar = new calendar.Calendar().monthdayscalendar(year, month)
    myCalendar.forEach(week => {
        row = []
        week.forEach(day => {
            if (day === 0) {
                row.push({ text: ' ', callback_data: 'ignore' })
            } else {
                row.push({ text: day, callback_data: `calendar-day-${day}-${month}-${year}` })
            }
        })
        markup.push(row)
    })

    row = []
    row.push({ text: '<', callback_data: `previous-month` })
    row.push({ text: ' ', callback_data: `ignore` })
    row.push({ text: '>', callback_data: `next-month` })
    markup.push(row)
    return markup
}

function monthNameModificator(month) {
    let monthName = 'Январь';

    switch (month) {
        case 1:
            monthName = 'Январь'
            break
        case 2:
            monthName = 'Февраль'
            break
        case 3:
            monthName = 'Март'
            break
        case 4:
            monthName = 'Апрель'
            break
        case 5:
            monthName = 'Май'
            break
        case 6:
            monthName = 'Июнь'
            break
        case 7:
            monthName = 'Июль'
            break
        case 8:
            monthName = 'Август'
            break
        case 9:
            monthName = 'Сентябрь'
            break
        case 10:
            monthName = 'Октябрь'
            break
        case 11:
            monthName = 'Ноябрь'
            break
        case 12:
            monthName = 'Декабрь'
            break
    }

    return monthName
}

function checkFullNumber(val) {
    return (val - val % 12) / 12;
}

module.exports.createCalendar = createCalendar;
