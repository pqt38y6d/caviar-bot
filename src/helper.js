const moment = require('moment');

const debug = (obj = {}) => {
    return JSON.stringify(obj, null, 4)
}

module.exports = {
    logStart() {
        console.log('Bot has been started ...')
    },

    getChatId(msg) {
        return msg.chat.id;
    },

    isDateRight(date) {
        return ((/^(?:[0-2][0-9]|[1-9]|3[01]).(?:0[1-9]|[1-9]|1[012]).(?:202[1-9]|2[1-9])\s(?:0[0-9]|[0-9]|1[0-9]|2[1234]):(?:[0-5][0-9])$/).test(date) && moment(date, "DD-MM-YYYY hh:mm:ss") > new Date())
    },

    formatDate(date) {
        return moment(date, "DD-MM-YYYY hh:mm:ss").format('YYYY-MM-DD HH:mm:ss');
    }
};



