const {GoogleSpreadsheet} = require('google-spreadsheet');
const {globalStorage} = require("./quickresto-requests");


GOOGLE_CREDENTINALS =
    {
        "type": "service_account",
        "project_id": "quickresto-report-generator",
        "private_key_id": "d03ef982496f927519ac6fb828f2c5aa302c7f57",
        "private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCu9sErVekqIqVY\nsGC6LRnWjeYVcrpcVHtcV3pBsKXCcSJkPwKaTgPB/kJlRnmKZL4xN7RMARl4hQ0V\nraGXiHR9nxAPUQL2yiBYOnq5zro/Wil3BJA47mIMTIKwb01P8tsHWCydpoD/x9Da\nGF5cZ5eIpOYJgjQkwaMyjc0q9a5YmIpKa0td/Ek6sVjVb94tjF5oDKTUNHWUdwCP\nNC7r6HhUNW6AYXLceLwCzhHQYoihDkIYIZNrIo52/TnHZdGAExmsyrNO/9kEpz3g\nUQyf8pUP4UDvCM/6Wwjqis8mINXr54FM2zjePSh7DTgTPqbR5JhMAAjVoDhEXlP4\nuWMVOrhrAgMBAAECggEAEE4OIGPulhU83W/9nTv/I9LjdaChPchHTfEYNpXBf/mB\n3/lM3Jf9zg7lnYFQA3w+vfo+XIFGhpTq0qT0rlkMEHmwFtodpMwFvNMhhSR+9mpu\nx/87EkmJn5j9t0Pk3r93qk6NEdmZ1rxTRtyggCUzQCVF2FSaJJbBbUBwgnSUw8xQ\nrL0oRExoFf7LtnfP1S5+8TZQxGp8ai19KOwBdh+84lNI6fmMTMnR7EuNGi9QNUIv\nEmP5J79IAkkfhiLA0OYltyf2pFZEHjN+vBzmcEm19UAq0zjMGRGfhMBw3v383ZM6\n/7T4usws5ECem0xNSimjV77s2JoRcg7XU/lf7/Pg0QKBgQDdFcySeZH528/kZDE+\npEBT/Yhc70eMKc19tlFjsVxi/bdQlq2eV7GYIr1kLF4vHI45mRG2t2tg3vwgGpgp\njiJwiAS9IA25pV47kjdbTD5HhjNcawPED623dAS1ludrHR0P2570i61m6mxSTNuh\nFRf5fQkL2vLnjn7rrLdzXL2AUwKBgQDKmFRNvkj6kuDyhAa8bwkFBWdVkfqmkvs5\nulSQX2iSS9jhgkinvwuEkCcWPdDcI3bOXifdnn72MtAKfY1O3LtgQ3YNaMK87LO7\nEnOwbA0sCjTTnZAHHmNrkc4AkUrgjWn8fWrkoxqHcUupj7TgkDhXSptZ2idMXNSP\nXuYu3/lEiQKBgD3n77uIHQ74tPK/92YR+g1KrddQDRm7TtYHqiyfKj5xytXfC0iO\nR6VQ/PVC0zCf71dDfElMfN3IMpMOv1LLgOs/uwv2IMG+A2/s1mFTLRE8C7+yG4DX\n3w3Ch9sNIUnGAjBBj/Xr8qchtykoaktlq6vpjhHqwh+CsBlv6IJ+EnYPAoGBAIcb\n040NS6of7fiN59gezIsWsXU1p00TVIuJIgAuQJTgFEFAG6gTFmFPqWGB1BobZI33\n6Oo5Wz5hYLC+TlC7kBKAqugCZ2yLBsnJYnHTfW1TWSuPNpAoyfDhCdsDCkZ6yKXk\nFjdoNrcA7Y6lEeF5DLnaPYRMokyg0/jRNv0nzRrRAoGBANoJ6RdEZ1FNs/FerdI4\nnepBfD5QvqcLSeTB31rK3EoXRBjotGSZiLXahACgydWGEBSz29oWmK0RRi7LS0jz\nedfDATlMLDhXHlgy+0yvCUQ4GJuQ/f1VApYNOhCeLRvyyRpmmGyeaKgEB3QlBGZW\nD9Fd/rsdw4SXDd4IJg9bStjL\n-----END PRIVATE KEY-----\n",
        "client_email": "caviar-bot@quickresto-report-generator.iam.gserviceaccount.com",
        "client_id": "102138977853023741826",
        "auth_uri": "https://accounts.google.com/o/oauth2/auth",
        "token_uri": "https://oauth2.googleapis.com/token",
        "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
        "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/caviar-bot%40quickresto-report-generator.iam.gserviceaccount.com"
    }


async function getGoogleData(data) {
    const doc = new GoogleSpreadsheet('16h4-GSF3SZWgBOW3wjoOGZCGHlkmhkhsdZuf0HSbrR0');
    await doc.useServiceAccountAuth(GOOGLE_CREDENTINALS);
    await doc.loadInfo();
    const sheet1 = doc.sheetsByIndex[0];
    await sheet1.loadCells();
    const rows = await sheet1.getRows();
    rows.map((i) => {
        i._rawData.map(async (item) => {
            let columnIndex = 0;
            switch (item) {
                case 'Кухня':
                    await setShiftValues(i, sheet1, globalStorage.kitchenTotal)
                case 'Бар':
                    await setShiftValues(i, sheet1, globalStorage.barTotal)
                case 'Завтраки':
                    await setShiftValues(i, sheet1, globalStorage.breakfastTotal)
                case 'Персонал кофе':
                    await setShiftValues(i, sheet1, globalStorage.stuffTotal)
                case 'Кальяны':
                    await setShiftValues(i, sheet1, globalStorage.hookahTotal)
                case 'Чайная карта':
                    await setShiftValues(i, sheet1, globalStorage.teaTotal)
                case 'Допы':
                    await setShiftValues(i, sheet1, globalStorage.addedTotal)
                case 'Выручка QuickResto':
                    await setShiftValues(i, sheet1, globalStorage.sumTotal)
                case 'Оплата картой (банковские чеки)':
                    await setShiftValues(i, sheet1, globalStorage.sumByCardTotal)
            }
        })
    })
    await sheet1.saveUpdatedCells();
}

async function setShiftValues(row, sheet, value) {
    let rowIndex = row._rowNumber - 1;
    let columnIndex = row._rawData.indexOf('');
    if (columnIndex < 0) {
        columnIndex = row._rawData.length;
    }
    let cell = sheet.getCell(rowIndex, columnIndex);
    cell.value = value;
}


module.exports.getGoogleData = getGoogleData
